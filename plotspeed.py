import os
import pickle
import time
import argparse
import uszipcode
import cartopy.crs as ccrs
import cartopy.feature as cf
from cartopy.io import shapereader
from cartopy.feature import ShapelyFeature

from mpl_toolkits.basemap import Basemap
import matplotlib.gridspec as gridspec
from matplotlib.collections import LineCollection
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex
from matplotlib.figure import Figure


class ZipcodeDatabase:
    """
    This class maps zipcodes to states.
    """
    saved_file = "ZipcodeDatabase.dat"

    def __init__(self):
        self.zipcode_to_state_mapping = dict()
        self._load_data()
        pass

    def state_from_zipcode(self, code):
        try:
            return self.zipcode_to_state_mapping[code]
        except KeyError:
            return None

    def group_by_region(self, codes):
        """
        Separate the zipcodes into three lists: continental US, Alaska, and Hawaii
        :param codes: Input zipcodes
        :return: A tuple of three lists, for Continental US, Alaska, and Hawaii
        """
        continental = []
        alaska = []
        hawaii = []
        for code in codes:
            st = self.state_from_zipcode(code)
            if not st:
                continue
            if st == 'AK':
                alaska.append(code)
            elif st == 'HI':
                hawaii.append(code)
            else:
                continental.append(code)
        return continental, alaska, hawaii

    @staticmethod
    def load_database():
        ZipcodeDatabase.ensure_saved_database()
        with open(ZipcodeDatabase.saved_file, 'rb') as fin:
            return pickle.load(fin)

    @staticmethod
    def ensure_saved_database():
        # If the file already exists, exit
        if os.path.isfile(ZipcodeDatabase.saved_file):
            return

        # Otherwise, create it
        sdb = ZipcodeDatabase()
        with open(ZipcodeDatabase.saved_file, 'wb') as fout:
            pickle.dump(sdb, fout)

    def _load_data(self):
        print("Building zipcode database.")

        # Load the zipcodes and map to state abbreviations
        self.zipcode_to_state_mapping = dict()
        search = uszipcode.SearchEngine()
        max_val = 100000
        for zip_num in range(501, max_val):
            # Create a 5-digit string for this number
            zip_str = str(zip_num).zfill(5)

            # Status update
            if zip_num % 500 == 0:
                print(f"   Loading Zipcode {zip_str} of {max_val}.")

            # Look up the state for this zipcode
            try:
                res = search.by_zipcode(zip_str)
                st_abr = res.state
                if st_abr is not None:
                    self.zipcode_to_state_mapping[zip_str] = st_abr
            except:
                pass


class ShapeDatabase:
    """
    This class manages the shape files for the states and zipcodes. It's intended to be pickled and saved
    so we save the overhead of loading every time the program runs.

    The first time the program runs, it will load all the shape data from CartoPy and the zipcode shape source. After
    that it will create a pickled version to use in the future.
    """
    saved_file = "ShapeDatabase.dat"
    zipcode_source_file = "zipcodedata/cb_2017_us_zcta510_500k.shp"

    def __init__(self):
        self.zipcode_geometries = dict()
        self.zipcode_shapes = dict()
        self.state_shapes = None
        self._load_shapes()

    @staticmethod
    def load_database():
        ShapeDatabase.ensure_saved_database()
        with open(ShapeDatabase.saved_file, 'rb') as fin:
            return pickle.load(fin)

    @staticmethod
    def ensure_saved_database():
        # If the file already exists, exit
        if os.path.isfile(ShapeDatabase.saved_file):
            return

        # Otherwise, create it
        sdb = ShapeDatabase()
        with open(ShapeDatabase.saved_file, 'wb') as fout:
            pickle.dump(sdb, fout)

    @property
    def all_zipcodes_present(self):
        return [code for code, _ in self.zipcode_geometries.items()]

    @property
    def zipcode_subset_small(self):
        return self.all_zipcodes_present[::20]

    @property
    def zipcode_subset_medium(self):
        return self.all_zipcodes_present[::5]

    @property
    def zipcode_subset_large(self):
        return self.all_zipcodes_present

    @property
    def zipcode_subset_battery(self):
        return [self.zipcode_subset_small, self.zipcode_subset_medium, self.zipcode_subset_large]
        #return [self.zipcode_subset_small]

    def shape_for_zipcodes(self, zipcodes):
        geoms = []
        for code in zipcodes:
            if code in self.zipcode_geometries:
                geoms.append(self.zipcode_geometries[code])
        return ShapelyFeature(geoms, ccrs.PlateCarree())

    def _load_shapes(self):
        print("Building shape database.")
        print("  Reading shape data")
        reader = shapereader.Reader(ShapeDatabase.zipcode_source_file)
        zip_geometry = [geom for geom in reader.geometries()]
        recs = list(reader.records())

        # Create a mapping from zipcode to shape
        self.zipcode_shapes = dict()
        for ii, rec in enumerate(recs):
            # Status update
            if ii % 500 == 0:
                print(f"   Loading Zipcode {ii} of {len(recs)}.")

            zipcode = rec.attributes['GEOID10']
            self.zipcode_geometries[zipcode] = zip_geometry[ii]
            self.zipcode_shapes[zipcode] = ShapelyFeature(zip_geometry[ii], ccrs.PlateCarree())

        # Load the state shapes
        self.state_shapes = cf.STATES.with_scale('50m')
        print("  Done.\n\n")


class ZipPlotter:
    """
    This class is the core plotter. It's based on this source:
    https://carlcolglazier.com/notes/plotting-2018-house-midterms-cartopy/

    It takes a list of zip codes and plots them over top of the United States with state boundaries.
    """

    _background_color = "#FFFFFF"
    _highlight_color = "#F8766D"

    def __init__(self):
        self.zipcode_database = ZipcodeDatabase.load_database()
        self.shape_database = ShapeDatabase.load_database()

        self.fig = None
        self.ax_continental = None
        self.ax_hawaii = None
        self.ax_alaska = None

        self._zip_plots = dict()

    def _set_axis_extents(self):
        self.ax_continental.set_extent([-125, -66.5, 20, 50])
        self.ax_hawaii.set_extent([-155, -165, 20, 15])
        self.ax_alaska.set_extent([-185, -130, 70, 50])

    def restore_canvas(self):
        """
        Restore the canvas after save and load
        :return: None
        """
        if not self.fig:
            return
        dummy = plt.figure()
        new_manager = dummy.canvas.manager
        new_manager.canvas.figure = self.fig
        self.fig.set_canvas(new_manager.canvas)
        self._set_axis_extents()

    def setup(self):
        self._setup_figure()
        self.set_axes()

    def set_axes(self):
        projection = ccrs.AlbersEqualArea(central_longitude=-100)
        self.ax_continental.projection = projection
        self.ax_hawaii.projection = projection
        self.ax_alaska.projection = projection

        self._set_axis_extents()

    def _setup_figure(self):
        # Create the figure
        if not self.fig:
            self.fig, _ = plt.subplots(figsize=(20, 15))
        else:
            self.fig.clear(keep_observers=True)

        # Set up a projection
        projection = ccrs.AlbersEqualArea(central_longitude=-100)

        # Continental United States
        self.ax_continental = self.fig.add_axes([-.05, -.05, 1.2, 1.2], projection=projection)
        self.ax_continental.add_feature(self.shape_database.state_shapes)
        self.ax_continental.set_extent([-125, -66.5, 20, 50])

        # Hawaii
        self.ax_hawaii = self.fig.add_axes([0.25, .1, 0.15, 0.15], projection=projection)
        self.ax_hawaii.add_feature(self.shape_database.state_shapes)
        self.ax_hawaii.set_extent([-155, -165, 20, 15])

        # Alaska
        self.ax_alaska = self.fig.add_axes([0.1, 0.1, 0.2, 0.2], projection=projection)
        self.ax_alaska.add_feature(self.shape_database.state_shapes)
        self.ax_alaska.set_extent([-185, -130, 70, 50])

        # Get rid of anything extra: boxes, backgrounds, etc.
        plt.box(False)
        # for subplot in [ax1, axak, axhi]:
        #    subplot.background_patch.set_visible(False)
        #    subplot.outline_patch.set_visible(False)

        self.fig.patch.set_visible(False)
        plt.axis('off')
        pass

    def _ax_from_state(self, state_abr):
        if state_abr == "HI":
            return self.ax_hawaii
        elif state_abr == "AK":
            return self.ax_alaska
        else:
            return self.ax_continental

    def plot_zipcodes_single(self, codes):
        """
        Plot each zipcode individually
        :param codes: Zipcodes to plot
        :return: None
        """
        # Setup the background figure
        self._setup_figure()

        # Plot the zipcodes
        for code in codes:
            zip_shape = self.shape_database.shape_for_zipcodes([code])
            abr = self.zipcode_database.state_from_zipcode(code)
            if not abr:
                continue
            ax = self._ax_from_state(abr)
            ax.add_feature(zip_shape, color=ZipPlotter._highlight_color, linewidth=0.00, edgecolor='w')
        pass

    def plot_zipcodes_group(self, codes):
        """
        Create a single object for each subplot
        :param codes: Zipcodes to plot
        :return: None
        """

        # Setup the background figure
        self._setup_figure()

        # Separate into groups
        codes_co, codes_ak, codes_hi = self.zipcode_database.group_by_region(codes)

        # Create a single shape for each group
        shape_co = self.shape_database.shape_for_zipcodes(codes_co)
        shape_ak = self.shape_database.shape_for_zipcodes(codes_ak)
        shape_hi = self.shape_database.shape_for_zipcodes(codes_hi)

        # Plot them
        self.ax_continental.add_feature(shape_co, color=ZipPlotter._highlight_color, linewidth=0.00, edgecolor='w')
        self.ax_alaska.add_feature(shape_ak, color=ZipPlotter._highlight_color, linewidth=0.00, edgecolor='w')
        self.ax_hawaii.add_feature(shape_hi, color=ZipPlotter._highlight_color, linewidth=0.00, edgecolor='w')
        pass

    def _setup_individual_plots(self):
        """
        Create an individual plot for every zipcode and store it. If they already exist, we don't need to do anything.
        :return: None
        """
        if self._zip_plots:
            return

        # Setup the background figure
        self._setup_figure()

        # Create individual plots for each zipcode.
        all_zips = self.shape_database.all_zipcodes_present
        self._zip_plots = dict()
        for code in all_zips:
            zip_shape = self.shape_database.shape_for_zipcodes([code])
            abr = self.zipcode_database.state_from_zipcode(code)
            if not abr:
                continue
            ax = self._ax_from_state(abr)
            self._zip_plots[code] = ax.add_feature(
                zip_shape, color=ZipPlotter._background_color, linewidth=0.00, edgecolor='w')

    def plot_zipcodes_color_change(self, codes):
        # Ensure that the base plots exist
        self._setup_individual_plots()

        # Change the colors according to the new codes
        for code, zip_plot in self._zip_plots.items():
            new_color = ZipPlotter._highlight_color if code in codes else ZipPlotter._background_color

            # Has this color changed?
            if zip_plot._kwargs['facecolor'] == new_color:
                continue

            # Update the color for this zipcode
            zip_plot._kwargs['edgecolor'] = new_color
            zip_plot._kwargs['facecolor'] = new_color
            zip_plot.pchanged()
            zip_plot.stale = True

        # Update the canvas
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        pass


def _test_setup(test_id, plotter, verbose=True):
    # Identify the correct plot function
    if test_id == 1:
        msg = "Testing Single-Zipcode plot."
        technique = "Individual-Shapes"
        plot_fn = plotter.plot_zipcodes_single
    elif test_id == 2:
        msg = "Testing Group-Zipcode plot."
        technique = "Grouped-Shapes"
        plot_fn = plotter.plot_zipcodes_group
    elif test_id == 3:
        msg = "Testing Color-Change-Zipcode plot."
        technique = "Individual-Shapes with color change"
        plot_fn = plotter.plot_zipcodes_color_change
    if verbose:
        print(msg)
    return technique, plot_fn


def test_plot_speed(test_id, big_first):
    # Setup the plotter
    print("Setup")
    plotter = ZipPlotter()

    # Create the datasets
    codesets = plotter.shape_database.zipcode_subset_battery
    if big_first:
        codesets.reverse()

    # Identify the correct plot function
    technique, plot_fn = _test_setup(test_id, plotter)

    # Plot everything
    direction = "Large-to-Small" if big_first else "Small-to-Large"
    print(f"Plotting first pass in {direction} order using {technique} method.")
    for codes in codesets:
        start = time.time()
        plot_fn(codes)
        plt.draw()
        plt.savefig(f"Pass1.{len(codes)}.png")
        print(f"  {len(codes)} zipcodes required {time.time() - start:.2f} seconds.")

    # Plot a second time to test the speed of re-plotting
    print(f"Plotting second pass in {direction} order using {technique} method.")
    for codes in codesets:
        start = time.time()
        plot_fn(codes)
        plt.draw()
        plt.savefig(f"Pass2.{len(codes)}.png")
        print(f"  {len(codes)} zipcodes zipcodes required {time.time() - start:.2f} seconds.")

    # Save the setup and re-load it
    print("Saving plotter and re-loading.")
    save_file = "SavePlotSetup.dat"
    with open(save_file, 'wb') as fout:
        pickle.dump(plotter, fout)
    del plotter
    with open(save_file, 'rb') as fin:
        plotter = pickle.load(fin)
        plotter.restore_canvas()
    technique, plot_fn = _test_setup(test_id, plotter, False)

    # Plot again using the reloaded data
    print(f"Plotting third pass in {direction} order using {technique} method after save and load.")
    for codes in codesets:
        start = time.time()
        plot_fn(codes)
        plt.draw()
        plt.savefig(f"Pass3.{len(codes)}.png")
        print(f"  {len(codes)} zipcodes zipcodes required {time.time() - start:.2f} seconds.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test the speed of various zipcode plotting techniques in CartoPy.')
    parser.add_argument('-t', '--test', type=int, default=1,
                        help="Pick which test to run (1, 2, or 3. Test 1 plots zipcodes individually"
                             "Test 2 plots zipcodes in blocks. Test 3 generates a single plot and changes color"
                             "depending on the selected zipcodes.")
    parser.add_argument('-b', '--bigfirst', action='store_true', help="Plot the larger tests first.")

    args = parser.parse_args()
    if args.test <= 3:
        test_plot_speed(args.test, args.bigfirst)

